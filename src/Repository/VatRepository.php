<?php

namespace App\Repository;

use App\Entity\Vat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTime;

/**
 * @method Vat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vat[]    findAll()
 * @method Vat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vat::class);
    }

    public function saveVat($name, $rate)
    {
        $vat = new Vat();
        
        $vat
            ->setName($name)
            ->setRate($rate)
            ->setCreatedAt(new DateTime());
        
        $this->_em->persist($vat);
        $this->_em->flush();
        
        return $vat;
    }
    
    public function updateVat(Vat $vat): Vat
    {
        $vat->setUpdatedAt(new DateTime());
        
        $this->_em->persist($vat);
        $this->_em->flush();
        
        return $vat;
    }
    
    public function deleteVat(Vat $vat)
    {
        $this->_em->remove($vat);
        $this->_em->flush();
    }
    
    // /**
    //  * @return Vat[] Returns an array of Vat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vat
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
