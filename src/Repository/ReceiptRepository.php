<?php

namespace App\Repository;

use App\Entity\Receipt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTime;

/**
 * @method Receipt|null find($id, $lockMode = null, $lockVersion = null)
 * @method Receipt|null findOneBy(array $criteria, array $orderBy = null)
 * @method Receipt[]    findAll()
 * @method Receipt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReceiptRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Receipt::class);
    }
    
    public function saveReceipt(): Receipt
    {
        $receipt = new Receipt();
        
        $date = new DateTime();
        
        $receipt
            ->setReceiptDate($date)
            ->setFinished(0)
            ->setCreatedAt($date);
        
        $this->_em->persist($receipt);
        $this->_em->flush();
        return $receipt;
    }
    
    public function updateReceipt(Receipt $receipt): Receipt
    {
        $receipt->setUpdatedAt(new DateTime());
        
        $this->_em->persist($receipt);
        $this->_em->flush();
        
        return $receipt;
    }
    
    public function getTurnoverPerHour(DateTime $date = null) {
        
        $conn = $this->_em->getConnection();
        
        $sql = '
            SELECT HOUR(r.created_at) AS `Hour`, SUM(ri.total) AS `Total` 
            FROM receipt r 
            JOIN receipt_item ri ON ri.receipt_id = r.id
            WHERE r.finished = 1';
        
        $params = [];
        
        if($date) {
            $sql .= ' AND DATE(r.created_at) = :date';
            $params = ['date' => $date->format('Y-m-d')];
        }
            
        $sql .= '     
            GROUP BY HOUR(r.created_at)
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute($params);
        
        return $stmt->fetchAllAssociative();
        
    }

    // /**
    //  * @return Receipt[] Returns an array of Receipt objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Receipt
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
