<?php

namespace App\Repository;

use App\Entity\ReceiptItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Receipt;
use App\Entity\Product;
use DateTime;

/**
 * @method ReceiptItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReceiptItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReceiptItem[]    findAll()
 * @method ReceiptItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReceiptItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReceiptItem::class);
    }
    
    public function saveReceiptItem(Receipt $receipt, Product $product, int $quantity, float $discountPercetage = 0): ReceiptItem
    {
        $item = new ReceiptItem();
        
        $item->setReceipt($receipt);
        $item->setProduct($product);
        $item->setQuantity($quantity);        
        $item->setPrice($product->getPrice());
        $item->setVatRate($product->getVat()->getRate());
        $item->setDiscountPercentage($discountPercetage);
        
        $item->calculateAmounts();
        
        $item->setCreatedAt(new DateTime());
        
        $this->_em->persist($item);
        $this->_em->flush();
        return $item;
    }
    
    public function updateReceiptItem(ReceiptItem $item): ReceiptItem
    {
        $item->calculateAmounts();
        
        $item->setUpdatedAt(new DateTime());
        
        $this->_em->persist($item);
        $this->_em->flush();
        return $item;
    }
    
    public function deleteReceiptItem(ReceiptItem $item)
    {
        $this->_em->remove($item);
        $this->_em->flush();
    }
    
    public function findByReceiptAndProduct(Receipt $receipt, Product $product)
    {
        return $this->createQueryBuilder('r')
        ->andWhere('r.receipt = :receipt')
        ->setParameter('receipt', $receipt)
        ->andWhere('r.product = :product')
        ->setParameter('product', $product)
        ->getQuery()
        ->getResult()
        ;
    }

    // /**
    //  * @return ReceiptItem[] Returns an array of ReceiptItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReceiptItem
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
