<?php

namespace App\Repository;

use App\Entity\Discount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTime;

/**
 * @method Discount|null find($id, $lockMode = null, $lockVersion = null)
 * @method Discount|null findOneBy(array $criteria, array $orderBy = null)
 * @method Discount[]    findAll()
 * @method Discount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DiscountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Discount::class);
    }

    public function saveDiscount(int $minimumProducts, float $percentage, 
        DateTime $validFrom, DateTime $validTo): Discount
    {
        $discount = new Discount();
        
        $discount
            ->setMinimumProducts($minimumProducts)
            ->setPercentage($percentage)
            ->setValidFrom($validFrom)
            ->setValidTo($validTo)
            ->setCreatedAt(new DateTime());
        
        $this->_em->persist($discount);
        $this->_em->flush();
        
        return $discount;
    }
    
    /**
     * @return Discount[] Returns an array of Discount objects
     */
    public function findByInterval(DateTime $validFrom, DateTime $validTo)
    {
        return $this->createQueryBuilder('d')        
            ->orWhere('d.valid_from <= :valid_from AND d.valid_to >= :valid_to')
            ->orWhere('d.valid_from >= :valid_from AND d.valid_to >= :valid_to')
            ->orWhere('d.valid_from >= :valid_from AND d.valid_to <= :valid_to')
            ->setParameter('valid_from', $validFrom->format('Y-m-d'))
            ->setParameter('valid_to', $validTo->format('Y-m-d'))
            ->getQuery()
            ->getResult()
        ;
    }
    
    public function findByDate(DateTime $date): ?Discount
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.valid_from <= :date')
            ->andWhere('d.valid_to >= :date')
            ->setParameter('date', $date->format('Y-m-d'))
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
}
