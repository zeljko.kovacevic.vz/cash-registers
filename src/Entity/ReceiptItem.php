<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReceiptItemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ReceiptItemRepository::class)
 */
class ReceiptItem implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Receipt::class, inversedBy="receiptItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $receipt;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="receiptItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $amount;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $discount_percentage;
    
    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $discount;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $amount_before_tax;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $vat_rate;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $vat;
    
    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $total;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;    

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReceipt(): ?Receipt
    {
        return $this->receipt;
    }

    public function setReceipt(?Receipt $receipt): self
    {
        $this->receipt = $receipt;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDiscountPercentage(): ?string
    {
        return $this->discount_percentage;
    }
    
    public function setDiscountPercentage(string $discount_percentage): self
    {
        $this->discount_percentage = $discount_percentage;
        
        return $this;
    }
    
    public function getDiscount(): ?string
    {
        return $this->discount;
    }

    public function setDiscount(string $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getAmountBeforeTax(): ?string
    {
        return $this->amount_before_tax;
    }

    public function setAmountBeforeTax(string $amount_before_tax): self
    {
        $this->amount_before_tax = $amount_before_tax;

        return $this;
    }

    public function getVatRate(): ?string
    {
        return $this->vat_rate;
    }

    public function setVatRate(string $vat_rate): self
    {
        $this->vat_rate = $vat_rate;

        return $this;
    }
    
    public function getVat(): ?string
    {
        return $this->vat;
    }
    
    public function setVat(string $vat): self
    {
        $this->vat = $vat;
        
        return $this;
    }

    public function getTotal(): ?string
    {
        return $this->total;
    }

    public function setTotal(string $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
    
    public function calculateAmounts()
    {
        $this->setAmount(round($this->getQuantity() * $this->getPrice(), 2));        
        $this->setDiscount(round($this->getAmount() * $this->getDiscountPercentage() / 100, 2));   
        $this->setAmountBeforeTax($this->getAmount() - $this->getDiscount());        
        $this->setVat(round($this->getAmountBeforeTax() * $this->getVatRate() / 100, 2));
        $this->setTotal($this->getAmountBeforeTax() + $this->getVat());
    }
    
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            //'receipt' => $this->receipt,
            'product' => $this->product,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'amount' => $this->amount,
            'discount_percentage' => $this->discount_percentage,
            'discount' => $this->discount,
            'amount_before_tax' => $this->amount_before_tax,
            'vat_rate' => $this->vat_rate,
            'total' => $this->total,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
    

    
}
