<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DiscountRepository;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=DiscountRepository::class)
 */
class Discount implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $minimum_products;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $percentage;

    /**
     * @ORM\Column(type="date")
     */
    private $valid_from;

    /**
     * @ORM\Column(type="date")
     */
    private $valid_to;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMinimumProducts(): ?int
    {
        return $this->minimum_products;
    }

    public function setMinimumProducts(int $minimum_products): self
    {
        $this->minimum_products = $minimum_products;

        return $this;
    }

    public function getPercentage(): ?string
    {
        return $this->percentage;
    }

    public function setPercentage(string $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function getValidFrom(): ?\DateTimeInterface
    {
        return $this->valid_from;
    }

    public function setValidFrom(\DateTimeInterface $valid_from): self
    {
        $this->valid_from = $valid_from;

        return $this;
    }

    public function getValidTo(): ?\DateTimeInterface
    {
        return $this->valid_to;
    }

    public function setValidTo(\DateTimeInterface $valid_to): self
    {
        $this->valid_to = $valid_to;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
    
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'minimumProducts' => $this->minimum_products,
            'percentage' => $this->percentage,
            'validFrom' => $this->valid_from,
            'validTo' => $this->valid_to,
        ];
    }

}
