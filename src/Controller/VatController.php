<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\VatRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class VatController
{
    
    /**
     * @var VatRepository
     */
    private $vatRepository;
    
    /**
     * @var ValidatorInterface
     */
    private $validator;
    
    public function __construct(VatRepository $vatRepository, ValidatorInterface $validator)
    {
        $this->vatRepository = $vatRepository;        
        $this->validator = $validator;
    }
    
    /**
     * @Route("/vat", name="add_vat", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        
        $errors = $this->validator->validate($data, new Assert\Collection([
            'name' => [
                new Assert\NotBlank(),
                new Assert\Type('string'),
                new Assert\Length(['min' => 3, 'max' => 100]),
            ],
            'rate' => [
                new Assert\NotBlank(),
                new Assert\Type('numeric'),
                new Assert\PositiveOrZero(),
                new Assert\LessThanOrEqual(100),
            ],
        ]));
        
        if(count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }
        
        $name = $data['name'];
        $rate = $data['rate'];
               
        $vat = $this->vatRepository->saveVat($name, $rate);
        
        return new JsonResponse([
            'data' => $vat,
        ], Response::HTTP_CREATED);
    }
    
    /**
     * @Route("/vat/{id}", name="update_vat", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse
    {
        $vat = $this->vatRepository->find($id);
        if(!$vat) {
            throw new NotFoundHttpException('VAT rate was not found!');
        }
        
        $data = json_decode($request->getContent(), true);
        
        $errors = $this->validator->validate($data, new Assert\Collection([
            'name' => [
                new Assert\NotBlank(),
                new Assert\Type('string'),
                new Assert\Length(['min' => 3, 'max' => 100]),
            ],
            'rate' => [
                new Assert\NotBlank(),
                new Assert\Type('numeric'),
                new Assert\PositiveOrZero(),
                new Assert\LessThanOrEqual(100),
            ],
        ]));
        
        if(count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }
        
        $vat->setName($data['name']);
        $vat->setRate($data['rate']);
                
        $updatedVat = $this->vatRepository->updateVat($vat);
        
        return new JsonResponse([
            'data' => $updatedVat,
        ], Response::HTTP_OK);
    }
    
    /**
     * @Route("/vat/{id}", name="get_vat", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function get($id): JsonResponse
    {
        $vat = $this->vatRepository->find($id);
        if(!$vat) {
            throw new NotFoundHttpException('VAT rate was not found!');
        }
        
        return new JsonResponse([
            'data' => $vat,
        ], Response::HTTP_OK);
    }
    
    /**
     * @Route("/vat", name="get_all_vat_rates", methods={"GET"})
     */
    public function getAll(): JsonResponse
    {
        $vats = $this->vatRepository->findAll();
        return new JsonResponse([
            'data' => $vats,
        ], Response::HTTP_OK);
    }
    
    /**
     * @Route("/vat/{id}", name="delete_vat", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete($id, Request $request): JsonResponse
    {
        $vat = $this->vatRepository->find($id);
        if(!$vat) {
            throw new NotFoundHttpException('VAT rate was not found!');
        }
        
        $this->vatRepository->deleteVat($vat);
        
        return new JsonResponse([], Response::HTTP_OK);
    }
    
}
