<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\ClientRepository;

class ClientsController
{
    
    /**
     * @var ClientRepository
     */
    private $clientRepository;        
    
    /**
     * @var ValidatorInterface
     */
    private $validator;
    
    public function __construct(
        ClientRepository $clientRepository, 
        ValidatorInterface $validator)
    {
        $this->clientRepository = $clientRepository;
        $this->validator = $validator;
    }
       
    /**
     * @Route("/clients", name="add_client", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        
        $errors = $this->validator->validate($data, new Assert\Collection([
            'name' => [
                new Assert\NotBlank(),
                new Assert\Type('string'),
                new Assert\Length(['min' => 3, 'max' => 180]),
            ],
            'role' => [
                new Assert\NotBlank(),
                new Assert\Type('json'),                
            ],            
        ]));
        
        if(count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }
        
        $name = $data['name'];
        $role = $data['role'];
        
        $client = $this->clientRepository->saveClient($name, [$role]);
        
        return new JsonResponse([
            'data' => $client,
        ], Response::HTTP_CREATED);
    }        
    
}
