<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Repository\VatRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ProductsController
{
    
    /**
     * @var ProductRepository
     */
    private $productRepository;
    
    /**
     * @var VatRepository
     */
    private $vatRepository;
    
    /**
     * @var ValidatorInterface
     */
    private $validator;
    
    public function __construct(
        ProductRepository $productRepository, 
        VatRepository $vatRepository,
        ValidatorInterface $validator)
    {
        $this->productRepository = $productRepository;
        $this->vatRepository = $vatRepository;
        $this->validator = $validator;
    }
       
    /**
     * @Route("/products", name="add_product", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        
        $errors = $this->validator->validate($data, new Assert\Collection([
            'vatId' => [
                new Assert\NotBlank(),
                new Assert\Type('integer'),
                new Assert\Positive(),
            ],
            'name' => [
                new Assert\NotBlank(),
                new Assert\Type('string'),
                new Assert\Length(['min' => 3, 'max' => 255]),
            ],
            'barcode' => [
                new Assert\NotBlank(),
                new Assert\Type('string'),
                new Assert\Length(['max' => 50]),
            ],
            'cost' => [
                new Assert\NotBlank(),
                new Assert\Type('numeric'),
                new Assert\PositiveOrZero(),
            ],
            'price' => [
                new Assert\NotBlank(),
                new Assert\Type('numeric'),
                new Assert\PositiveOrZero(),
            ],
        ]));
        
        if(count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }
        
        $barcode = $data['barcode'];
        $existingProduct = $this->productRepository->findOneBy(['barcode' => $barcode]);
        if($existingProduct) {
            throw new BadRequestHttpException('Product already exists with this barcode');
        }
        
        $vatId = $data['vatId'];
        $vat = $this->vatRepository->find($vatId);
        if(!$vat) {
            throw new BadRequestHttpException('Invalid VAT id!');
        }
                
        $name = $data['name'];
        $cost = $data['cost'];
        $price = $data['price'];                        
        
        $product = $this->productRepository->saveProduct($barcode, $name, $cost, $price, $vat);
        
        return new JsonResponse([
            'data' => $product,
        ], Response::HTTP_CREATED);
    }
    
    /**
     * @Route("/products", name="get_all_products", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getAll(): JsonResponse
    {
        $products = $this->productRepository->findAll();
        return new JsonResponse([
            'data' => $products,
        ], Response::HTTP_OK);
    }
    
    /**
     * @Route("/products/{barcode}", name="get_product", methods={"GET"})
     */
    public function get($barcode): JsonResponse
    {
        $product = $this->productRepository->findOneBy(['barcode' => $barcode]);
        if(!$product) {
            throw new NotFoundHttpException('Product was not found!');
        }
        
        return new JsonResponse([
            'data' => $product,
        ], Response::HTTP_OK);
    }        
    
}
