<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use App\Repository\ReceiptRepository;
use App\Repository\ReceiptItemRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Exception;
use DateTime;
use App\Repository\DiscountRepository;

class ReceiptsController
{
    
    /**
     * @var ReceiptRepository
     */
    private $receiptRepository;
    
    /**
     * @var ReceiptItemRepository
     */
    private $receiptItemRepository;
    
    /**     
     * @var ProductRepository
     */
    private $productRepository;
    
    /**
     * @var DiscountRepository
     */
    private $discountRepository;
    
    /**     
     * @var ValidatorInterface
     */
    private $validator;
    
    public function __construct(
        ReceiptRepository $receiptRepository, 
        ReceiptItemRepository $receiptItemRepository, 
        ProductRepository $productRepository,
        DiscountRepository $discountRepository,
        ValidatorInterface $validator)
    {
        $this->receiptRepository = $receiptRepository;
        $this->receiptItemRepository = $receiptItemRepository;
        $this->productRepository = $productRepository;
        $this->discountRepository = $discountRepository;
        $this->validator = $validator;
    }
            
    /**
     * @Route("/receipts", name="add_receipt", methods={"POST"})
     */
    public function create(Request $request): JsonResponse
    {
        $receipt = $this->receiptRepository->saveReceipt();
        
        return new JsonResponse([
            'data' => $receipt,            
        ], Response::HTTP_CREATED);
    }
    
    /**
     * @Route("/receipts/{id}", name="update_receipt", methods={"PATCH"})
     */
    public function update($id, Request $request): JsonResponse
    {
        $receipt = $this->receiptRepository->find($id);
        if(!$receipt) {
            throw new NotFoundHttpException('Receipt was not found!');
        }
        
        if($receipt->getFinished()) {
            throw new BadRequestHttpException('Receipt is already finished!');
        }
        
        $items = $receipt->getReceiptItems();
        if($items->count() == 0) {
            throw new BadRequestHttpException('Cannot finish receipt without products added!');
        }
        
        $data = json_decode($request->getContent(), true);        
                
        $errors = $this->validator->validate($data, new Assert\Collection([
            'finished' => [
                new Assert\NotBlank(),
                new Assert\IsTrue(),
            ],
        ]));
        
        if(count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }
        
        $finished = isset($data['finished']);  
        $receipt->setFinished($finished);
        
        $updatedReceipt = $this->receiptRepository->updateReceipt($receipt);
        
        return new JsonResponse([
            'data' => $updatedReceipt,
        ], Response::HTTP_OK);
    }
    
    /**
     * @Route("/receipts/{id}/add-product", name="add_receipt_product", methods={"POST"})
     */
    public function addProduct($id, Request $request): JsonResponse
    {                
        $receipt = $this->receiptRepository->find($id);
        if(!$receipt) {
            throw new NotFoundHttpException('Receipt was not found!');
        }
        
        $data = json_decode($request->getContent(), true);
        
        $errors = $this->validator->validate($data, new Assert\Collection([
            'barcode' => [
                new Assert\NotBlank(),     
                new Assert\Type('string'),
            ],
            'quantity' => [
                new Assert\NotBlank(),  
                new Assert\Type('integer'),
                new Assert\Positive(),
            ],
        ]));
        
        if(count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }
        
        if($receipt->getFinished()) {
            throw new BadRequestHttpException('Receipt is already finished!');
        }
        
        $barcode = $data['barcode'];
        $quantity = $data['quantity'];
        
        $product = $this->productRepository->findOneBy(['barcode' => $barcode]);
        if(!$product) {
            throw new NotFoundHttpException('Product was not found!');
        }
        
        $existing = $this->receiptItemRepository->findByReceiptAndProduct($receipt, $product);
        if($existing) {
            throw new BadRequestHttpException("Product with barcode $barcode already added!");
        }                                     
        
        $discount = $this->discountRepository->findByDate(new DateTime());
        $totalItems = $this->receiptItemRepository->count(['receipt' => $receipt]);                
        
        $discountPercentage = 0;        
        if($discount && $totalItems >= $discount->getMinimumProducts()) {
            $discountPercentage = $discount->getPercentage();
        }
        
        $receiptItem = $this->receiptItemRepository->saveReceiptItem($receipt, $product, $quantity, $discountPercentage);
        
        return new JsonResponse([
            'data' => $receiptItem,
        ], Response::HTTP_CREATED);
    }
    
    /**
     * @Route("/receipts/{id}/update-last-product", name="update_last_receipt_product", methods={"PUT"})
     */
    public function updateLastProduct($id, Request $request): JsonResponse
    {        
        
        $receipt = $this->receiptRepository->find($id);
        if(!$receipt) {
            throw new NotFoundHttpException('Receipt was not found!');
        }                
        
        if($receipt->getFinished()) {
            throw new BadRequestHttpException('Receipt is already finished!');
        }
        
        $items = $receipt->getReceiptItems();
        if($items->count() == 0) {
            throw new BadRequestHttpException('There are not products added to this receipt!');
        }
        
        $data = json_decode($request->getContent(), true);
        
        $errors = $this->validator->validate($data, new Assert\Collection([
            'quantity' => [
                new Assert\NotBlank(),
                new Assert\Type('integer'),
                new Assert\Positive(),
            ],
        ]));
        
        if(count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }
        
        $receiptItem = $items->last();        
        $receiptItem->setQuantity($data['quantity']);
        
        $updatedReceiptItem = $this->receiptItemRepository->updateReceiptItem($receiptItem);
        
        return new JsonResponse([
            'data' => $updatedReceiptItem,
        ], Response::HTTP_OK);
    }       
    
    /**
     * @Route("/receipts/{id}/delete-product", name="delete_receipt_product", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteProduct($id, Request $request): JsonResponse
    {        
        $receipt = $this->receiptRepository->find($id);
        if(!$receipt) {
            throw new NotFoundHttpException('Receipt was not found!');
        }
                        
        if($receipt->getFinished()) {
            throw new BadRequestHttpException('Receipt is already finished!');
        }
     
        $data = json_decode($request->getContent(), true);
        
        $errors = $this->validator->validate($data, new Assert\Collection([
            'barcode' => [
                new Assert\NotBlank(),
                new Assert\Type('string'),
            ],            
        ]));
        
        if(count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }
        
        $barcode = $data['barcode'];
        
        $deleted = false;
        $items = $receipt->getReceiptItems();
        foreach($items as $item) {
            if($item->getProduct()->getBarcode() == $barcode) {
                $this->receiptItemRepository->deleteReceiptItem($item);
                $deleted = true;
                break;
            }
        }
        
        if(!$deleted) {
            throw new NotFoundHttpException("Product with barcode $barcode was not found!");
        }
        
        return new JsonResponse([
            'data' => $receipt,
        ], Response::HTTP_OK);
    }
    
    /**
     * @Route("/receipts/{id}", name="get_receipt", methods={"GET"})
     */
    public function get($id): JsonResponse
    {
        $receipt = $this->receiptRepository->find($id);
        if(!$receipt) {
            throw new NotFoundHttpException('Receipt was not found!');
        }
        
        $vatSummary = [];
        
        $_items = [];
        $total = 0;
        
        $items = $receipt->getReceiptItems();
        foreach($items as $item) {
            $vatTotal = $vatSummary[$item->getVatRate()] ?? 0;
            $vatSummary[$item->getVatRate()] = $vatTotal + $item->getVat();
            
            $total += $item->getTotal();
            $_items[] = $item;
        }
        
        $vatSpecification = [];
        foreach($vatSummary as $key => $value) {
            $vatSpecification[] = [
                'rate' => $key,
                'total' => $value,
            ];            
        }
        
        $data = [
            'receipt' => $receipt,
            'items' => $_items,
            'total' => $total,
            'vatSpecification' => $vatSpecification,            
        ];
        
        return new JsonResponse([
            'data' => $data,
        ], Response::HTTP_OK);
    }   
    
    /**
     * @Route("/receipts-turnover/{date}", name="get_receipts_turnover", defaults={"date" = null}, methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function turnover($date = null): JsonResponse
    {
        $dateObj = null;
        
        if($date != null) {
            try {
                $dateObj = new DateTime($date);
            } catch(Exception $e) {
                throw new BadRequestHttpException('Invalid date specified');
            }
        }
        
        $turnoverPerHour = $this->receiptRepository->getTurnoverPerHour($dateObj);
        
        return new JsonResponse([
            'data' => $turnoverPerHour,
        ], Response::HTTP_OK);
    }   
    
}
