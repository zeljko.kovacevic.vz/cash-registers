<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\ClientRepository;
use App\Repository\DiscountRepository;
use DateTime;

class DiscountsController
{
    
    /**
     * @var DiscountRepository
     */
    private $discountRepository;        
    
    /**
     * @var ValidatorInterface
     */
    private $validator;
    
    public function __construct(
        DiscountRepository $discountRepository, 
        ValidatorInterface $validator)
    {
        $this->discountRepository = $discountRepository;
        $this->validator = $validator;
    }
       
    /**
     * @Route("/discounts", name="add_discount", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        
        $errors = $this->validator->validate($data, new Assert\Collection([
            'minimumProducts' => [
                new Assert\NotBlank(),
                new Assert\Type('integer'),
                new Assert\Length(['min' => 1]),
            ],
            'percentage' => [
                new Assert\NotBlank(),
                new Assert\Type('numeric'),
                new Assert\Positive(),
            ],            
            'validFrom' => [
                new Assert\NotBlank(),
                new Assert\Date(),                
            ],
            'validTo' => [
                new Assert\NotBlank(),
                new Assert\Date(),
            ],
        ]));
        
        if(count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }
        
        $validFrom = new DateTime($data['validFrom']);
        $validTo = new DateTime($data['validTo']);
                
        $existing = $this->discountRepository->findByInterval($validFrom, $validTo);
        if(count($existing) > 0) {
            throw new BadRequestHttpException('There is already defined discount in the same period!');
        }
        
        $minimumProducts = $data['minimumProducts'];
        $percentage = $data['percentage'];
        
        $discount = $this->discountRepository->saveDiscount($minimumProducts, $percentage, $validFrom, $validTo);
        
        return new JsonResponse([
            'data' => $discount,
        ], Response::HTTP_CREATED);
    }        
    
}
