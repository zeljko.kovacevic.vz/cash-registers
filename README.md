# Installation

Download all the packages:  
`composer install`  

Configure the .env file with the proper database settings.  

Run the migrations:  
`bin/console doctrine:migrations:migrate`  

Two user/clients accounts will be automatically created:  
Admin with the token: `1469f0770229f28f7762614ed78282806adf15ac`  
Cash register with the token: `1469f0770229f28f7762614ed78282806adf15ac`  

# Usage

Start the local server:
`symfony server:start`

You can test the API with the following requests in some REST test client (e.g. [Advanced REST client](https://chrome.google.com/webstore/detail/advanced-rest-client/hgmloofddffdnphfgcellkdfbfbjeloo?hl=hr))  

You should configure your REST client to send `x-auth-token` header with the one of the token values created in the first step.  

## Create VAT ##
POST /vat  
X-AUTH-TOKEN: "1469f0770229f28f7762614ed78282806adf15ac"  
{  
	"name": "vat name",  
	"rate": 21  
}  

## Update VAT ##  
PUT /vat/{VAT_ID}  
{  
	"name": "vat name",  
	"rate": 21  
}  

## Get VAT ##  
GET /vat/{VAT_ID}  

## Delete VAT ##  
DELETE /vat/{VAT_ID}  



## Create product ##  
POST /products  
X-AUTH-TOKEN: "1469f0770229f28f7762614ed78282806adf15ac"  
{  
  "barcode": "123456789",  
  "vatId": 1,  
  "name": "Bananas",  
  "cost": 10,  
  "price": 15  
}  

## Get all products  
GET /products  
X-AUTH-TOKEN: "1469f0770229f28f7762614ed78282806adf15ac"  

## Get product  
GET /products/{PRODUCT_ID}  


## Create discount ##  
POST /discounts  
X-AUTH-TOKEN: "1469f0770229f28f7762614ed78282806adf15ac"  
{  
	"minimumProducts": 2,  
	"percentage": 100,  
	"validFrom": "2021-02-08",  
	"validTo": "2021-02-15"  
}  


## Create recepit ##  
POST /receipts  
X-AUTH-TOKEN: "dd22930f8eccac1f9dac27cc0dd74d7b075d8c94"  
{}  

## Mark the receipt as finished ##  
PATCH /receipts/{RECEIPT_ID}  
X-AUTH-TOKEN: "dd22930f8eccac1f9dac27cc0dd74d7b075d8c94"  
{  
	"finished": true  
}  

## Add product to the receipt  
POST /receipts/{RECEIPT_ID}/add-product  
X-AUTH-TOKEN: "dd22930f8eccac1f9dac27cc0dd74d7b075d8c94"  
{  
	"barcode": "123456789",  
	"quantity": 1  
}  

## Update last product  
PUT /receipts/{RECEIPT_ID}/update-last-product  
X-AUTH-TOKEN: "dd22930f8eccac1f9dac27cc0dd74d7b075d8c94"  
{  
	"quantity": 2  
}  

## Delete product from receipt  
DELETE /receipts/{RECEIPT_ID}/delete-product  
X-AUTH-TOKEN: "1469f0770229f28f7762614ed78282806adf15ac"  
{  
	"barcode": "12345679"  
}  

## Get the receipt  
GET /receipts/{RECEIPT_ID}  
X-AUTH-TOKEN: "dd22930f8eccac1f9dac27cc0dd74d7b075d8c94"  

## Get turnover per hour for date  
GET /receipts-turnover/{date}  
X-AUTH-TOKEN: "1469f0770229f28f7762614ed78282806adf15ac"  

## Get all time turnover per hour  
GET /receipts-turnover  
X-AUTH-TOKEN: "1469f0770229f28f7762614ed78282806adf15ac"  




  
  
  
