<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210208194600 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT INTO `client` (`id`, `name`, `roles`, `token`) VALUES
            (1, 'Admin', '[\"ROLE_ADMIN\"]', '1469f0770229f28f7762614ed78282806adf15ac'),
            (2, 'Cash register', '[\"ROLE_CASH_REGISTER\"]', 'dd22930f8eccac1f9dac27cc0dd74d7b075d8c94');");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DELETE FROM client');
    }
}
