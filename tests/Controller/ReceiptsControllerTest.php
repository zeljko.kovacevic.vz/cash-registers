<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ReceiptsControllerTest extends WebTestCase
{

    public function testReceiptsCreateAndUpdate()
    {
        $client = static::createClient();
        
        // Create a receipt
        $client->request(
            'POST',
            '/receipts',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json', 
                'X-AUTH-TOKEN' => '123456789',                
            ],
            ''
        );        
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $json = $client->getResponse()->getContent();        
        $this->assertJson($json);
        
        $data = json_decode($json, true);
        $receipt = $data['data'];
        
        // Try to finish the receipt without products        
        $client->request(
            'PATCH',
            '/receipts/' . $receipt['id'],
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'X-AUTH-TOKEN' => '123456789',
            ],
            '{ "finished": true }'
            );
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }
    
}

